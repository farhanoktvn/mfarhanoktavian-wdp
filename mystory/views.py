from django.shortcuts import (
    render,
    HttpResponse,
    get_object_or_404,
    redirect,
)
from django.views import generic
from .models import (
    Friend,
    ClassYear,
)
from .forms import (
    FriendForm
)
import json

def index(request):
    response = {}
    return render(request, 'index.html', response)

def about(request):
    response = {}
    return render(request, 'about.html', response)

def projects(request):
    response = {}
    return render(request, 'projects.html', response)

def contact(request):
    response = {}
    return render(request, 'contact.html', response)

class Friends(generic.ListView):
    model = Friend
    template_name = 'friend-list.html'
    context_object_name = 'friends'

def add_friend(request):
    form = FriendForm(request.POST or None)
    if (request.method == "POST"):
        print("post")
        print(form)
        if form.is_valid():
            print("form_valid")
            form.save()
        return redirect('friend_list')
    else:
        form = FriendForm()
    years = [(y[0], 2021-int(y[0])) for y in ClassYear.objects.all().values_list('year')]
    response = {
        'form': form,
        'year_list': years,
    }
    return render(request, 'add-friend.html', response)

def delete_friend(request, *args, **kwargs):
    Friend.objects.get(id=kwargs.get('friend_id')).delete()
    return redirect('friend_list')
