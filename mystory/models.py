from django.db import models
from django.contrib.auth.models import User

class ClassYear(models.Model):
    # Year identity
    year = models.CharField(max_length=4, unique=True)

    # Object Data
    created_on = models.DateTimeField(auto_now=True)

    def __str__(self):
        return self.year

    class Meta:
        ordering = ('year',)
        verbose_name = 'year'
        verbose_name_plural = 'years'

class Friend(models.Model):
    # Friend Identity
    name = models.CharField(max_length=200, unique=True)
    year = models.ForeignKey(ClassYear, on_delete=models.CASCADE)
    hobby = models.CharField(max_length=100)
    favourite_food = models.CharField(max_length=75)
    favourite_beverage = models.CharField(max_length=75)

    # Object Data
    updated_on = models.DateTimeField(auto_now=True)
    created_on = models.DateTimeField(auto_now_add=True)

    def __str__(self):
        return self.name

    class Meta:
        ordering = ('name',)
        verbose_name = 'friend'
        verbose_name_plural = 'friends'
