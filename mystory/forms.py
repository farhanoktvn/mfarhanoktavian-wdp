from django import forms
from .models import (
    Friend,
    ClassYear,
    )

class FriendForm(forms.ModelForm):

    class Meta:
        model = Friend
        fields = ('name', 'year', 'hobby', 'favourite_food', 'favourite_beverage')
