from . import views
from django.urls import path, include

urlpatterns = [
    path('', views.index, name='index'),
    path('about/', views.about, name='about'),
    path('projects/', views.projects, name='projects'),
    path('contact/', views.contact, name='contact'),
    path('friends/list/', views.Friends.as_view(), name='friend_list'),
    path('friends/add/', views.add_friend, name='add_friend'),
    path('friends/delete/<friend_id>', views.delete_friend, name='delete_friend'),
]
